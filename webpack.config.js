var webpack = require('webpack');
var path = require("path");
let cfg = {
    context: __dirname,
    entry: ['./src/main.js'],
    output: {
        path: path.join(__dirname, "dist"),
        filename: "bundle.js",
        publicPath: 'http://localhost:8080/dist'
    },
    devtool: 'eval',
    module: {
        preLoaders: [
            {test: /\.js$/, loader: "eslint-loader", exclude: /node_modules/}
        ],
        loaders: [
            {test: /\.js$/, loaders: ["babel-loader"], exclude: /node_modules/},
            {
                test: /\.json?$/,
                loader: 'json-loader'
            },
            {
                test: /\.scss$/,
                include: /src/,
                loaders: [
                    'style',
                    'css',
                    'autoprefixer?browsers=last 3 versions',
                    'sass?outputStyle=expanded'
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'url?limit=8192',
                    'img'
                ]
            },
        ]
    },
    plugins: [
    ],
    resolve: {
        alias: {
            knockout: path.join(path.resolve(__dirname),'node_modules/knockout/build/output/knockout-latest'),
        },
    },
    eslint: {
        configFile: './.eslintrc',
        fix: true,
        //formatter: require("eslint-friendly-formatter"),
        formatter: require("eslint/lib/formatters/stylish"),
        emitError: false
    }
};

// cfg.resolve.alias.knockout = 'node_modules/knockout/build/output/knockout-latest'

if (process.env.NODE_ENV === 'production') {
//     cfg.plugins.push(new webpack.optimize.CommonsChunkPlugin('main', 'bundle.js', Infinity));
}else{
    if (process.env.NODE_ENV !== 'production') {
//        cfg.entry.push('webpack-dev-server/client?http://localhost:8080');
        cfg.entry.push('webpack/hot/only-dev-server');
    }
}


module.exports = cfg;
